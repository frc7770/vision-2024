# Vision Coprocessor
The code that runs on the RaspberryPi WPILibPi coprocessor and shares info back to the Network Tables.

**This must be compiled with 2023's VSCode.**

```
// To build, run the following in a terminal
.\gradlew build -Pvision 
// creates build/libs/vision-2024-raspberrypi.jar
```
Then upload through the WPILibPi interface Application page.

## Accessing WPILibPi in the robot
* The Pi will be wired to the radio using an ethernet cord.
* Join the radio's wifi as you would to drive the bot
* In a browser open http://wpilibpi.local/
* Upload your new jar on the Application page
* Use the Vision Status page to see your debug terminal output.

# Setting up development environment
Follow the setup instructions for WPILibPi [HERE](https://docs.wpilib.org/en/stable/docs/software/vision-processing/wpilibpi/index.html)

But before you do note that there are two ways to install and the WPILibPi image on you RaspberryPi.  
* The regular WPILibPi is set up to be a vision coprocessor using ethernet only.  WiFi is completely disabled as it is not legal for use on the rebot.  It's fine to use this method at home if your Pi will be hard wired into the same network with your development machine.
* It is usually easier to set up your development Pi using the WPILibPi-Romi image found [HERE](https://docs.wpilib.org/en/stable/docs/romi-robot/imaging-romi.html).  This image is set up for Romi robot development and therefore hosts its own WiFi network to connect your development machine too.  This also allows you to use the Vision-2024 project to run a robot in Simulation mode to test Network Tables interactions.  (ToDo: write more details about how to do this.)

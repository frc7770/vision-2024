package frc.vision;

import java.util.ArrayList;

import org.opencv.core.MatOfPoint;

import edu.wpi.first.vision.VisionPipeline;

public interface ContourDetection extends VisionPipeline {
    public ArrayList<MatOfPoint> filterContoursOutput();
}

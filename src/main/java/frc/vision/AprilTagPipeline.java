package frc.vision;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import edu.wpi.first.apriltag.AprilTagDetection;
import edu.wpi.first.apriltag.AprilTagDetector;
import edu.wpi.first.apriltag.AprilTagPoseEstimator;
import edu.wpi.first.apriltag.AprilTagPoseEstimator.Config;
import edu.wpi.first.math.ComputerVisionUtil;
import edu.wpi.first.math.geometry.CoordinateSystem;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.Topic;
import edu.wpi.first.vision.VisionPipeline;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * Example pipeline.
 */
public class AprilTagPipeline implements VisionPipeline {
  public int val;
  public int cameraId;
  public String cameraName;

  // Set up Pose Estimator - parameters are for a Microsoft Lifecam HD-3000
  // (https://www.chiefdelphi.com/t/wpilib-apriltagdetector-sample-code/421411/21)

  // Set up Pose Estimator - parameters are for a Microsoft Lifecam HD-3000
  // fx camera horizontal focal length, in pixels
  // fy camera vertical focal length, in pixels
  // cx camera horizontal focal center, in pixels
  // cy camera vertical focal center, in pixels
  double cameraFx = 699.377810315881;
  double cameraFy = 677.7161226393544;
  double cameraCx = 345.6059345433618;
  double cameraCy = 207.12741326228522;

  double tagSize = 0.1524; // meters

  // (https://www.chiefdelphi.com/t/wpilib-apriltagdetector-sample-code/421411/21)
  Config poseEstConfig = new AprilTagPoseEstimator.Config(tagSize, cameraFx, cameraFy, cameraCx, cameraCy);

  AprilTagPoseEstimator estimator = new AprilTagPoseEstimator(poseEstConfig);

  private final Field2d m_field = new Field2d();

  private NetworkTableInstance inst = NetworkTableInstance.getDefault(); 


  public AprilTagPipeline(int id, String name) {
    cameraId = id;
    cameraName = name;
    SmartDashboard.putData("AprilTag "+id,m_field);
  }

  @Override
  public void process(Mat mat) {
    //System.out.println("AprilTagPipeline: " + cameraName);
    var detector = new AprilTagDetector();
    // look for tag16h5, don't correct any error bits
    detector.addFamily("tag36h11", 0);
    val += 1;

    var grayMat = new Mat();
    Imgproc.cvtColor(mat, grayMat, Imgproc.COLOR_RGB2GRAY);

    AprilTagDetection[] detections = detector.detect(grayMat);
    float quality = 0;
    for (AprilTagDetection detection : detections) {
      quality = detection.getDecisionMargin();
      System.out.println(this.cameraName + " sees " + detection.getId() + " at quality " + quality);
      //System.out.println("AprilTagPipeline Detection: " + detection);
      // determine pose
      Transform3d pose = estimator.estimate(detection);
      System.out.println("AprilTagPipeline Pose: " + pose);

      Pose3d tagInFieldFrame;// = VisionMain.aprilTagFieldLayout.getTags().get(detection.getId()).pose; // pose from WPILib resource

      if (VisionMain.aprilTagFieldLayout.getTagPose(detection.getId()).isPresent()
          && detection.getDecisionMargin() > 50.) // margin < 20 seems bad; margin > 120 are good
      {
        tagInFieldFrame = VisionMain.aprilTagFieldLayout.getTagPose(detection.getId()).get();
        System.out.println(tagInFieldFrame);
      } else {
        System.out.println("bad id " + detection.getId() + " " + detection.getDecisionMargin());
        continue;
      }

      // remember we saw this tag
      // tags.add((long) detection.getId());

      // These transformations are required for the correct robot pose.
      // They arise from the tag facing the camera thus Pi radians rotated or CCW/CW
      // flipped from the
      // mathematically described pose from the estimator that's what our eyes see.
      // The true rotation
      // has to be used to get the right robot pose.
      pose = new Transform3d(
          new Translation3d(
              pose.getX(),
              pose.getY(),
              pose.getZ()),
          new Rotation3d(
              -pose.getRotation().getX() - Math.PI,
              -pose.getRotation().getY(),
              pose.getRotation().getZ() - Math.PI));

      // OpenCV and WPILib estimator layout of axes is EDN and field WPILib is NWU;
      // need x -> -y , y -> -z , z -> x and same for differential rotations
      // pose = CoordinateSystem.convert(pose, CoordinateSystem.EDN(),
      // CoordinateSystem.NWU());
      // WPILib convert is wrong for transforms as of 2023.4.3 so use this patch for
      // now
      // {
      // corrected convert
      var from = CoordinateSystem.EDN();
      var to = CoordinateSystem.NWU();
      pose = new Transform3d(
          CoordinateSystem.convert(pose.getTranslation(), from, to),
          CoordinateSystem.convert(new Rotation3d(), to, from)
              .plus(CoordinateSystem.convert(pose.getRotation(), from, to)));
      // end of corrected convert
      // }

      /*
       * var // transform to camera from robot chassis center at floor level
       * cameraInRobotFrame = new Transform3d(
       * // new Translation3d(0., 0., 0.),// camera at center bottom of robot zeros
       * for test data
       * // new Rotation3d(0.0, Units.degreesToRadians(0.),
       * Units.degreesToRadians(0.0))); // camera in line with robot chassis
       * new Translation3d(0.2, 0., 0.8),// camera in front of center of robot and
       * above ground
       * new Rotation3d(0.0, Units.degreesToRadians(-30.),
       * Units.degreesToRadians(0.0))); // camera in line with robot chassis
       * // y = -30 camera points up; +30 points down; sign is correct but backwards
       * of LL
       */
      Transform3d cameraInRobotFrame = VisionMain.cameraMounts[cameraId];

      // robot in field is the composite of 3 pieces
      var robotInFieldFrame = ComputerVisionUtil.objectToRobotPose(tagInFieldFrame, pose, cameraInRobotFrame);
      // end transforms to get the robot pose from this vision tag pose
      System.out.println("AprilTagPipeline robotInFieldFrame: "+robotInFieldFrame);

      // put pose into dashboard
      Rotation3d rot = robotInFieldFrame.getRotation();
      m_field.setRobotPose(robotInFieldFrame.toPose2d());
      sendPose(robotInFieldFrame);
      
      /*tagsTable
          .getEntry("pose_" + detection.getId())
          .setDoubleArray(
              new double[] {
                  robotInFieldFrame.getX(), robotInFieldFrame.getY(), robotInFieldFrame.getZ(),
                  rot.getX(), rot.getY(), rot.getZ()
              });*/
       
    }

    // put list of tags onto dashboard
    // pubTags.set(tags.stream().mapToLong(Long::longValue).toArray());
    // }
  }

  private void sendPose(Pose3d robotPose) {
    double[] nums = {robotPose.getX(),
      robotPose.getY(),
      robotPose.getZ(),
      robotPose.getRotation().getX(),  // roll
      robotPose.getRotation().getY(),  // pitch
      robotPose.getRotation().getZ()}; // yaw
    SmartDashboard.putNumberArray("AprilTag "+cameraId+" Pose", nums);
  }
}
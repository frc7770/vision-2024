package frc.vision;

import java.util.ArrayList;
import java.util.ResourceBundle.Control;

import javax.sound.sampled.Port;

import org.opencv.core.KeyPoint;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import edu.wpi.first.cscore.VideoSource;
import edu.wpi.first.vision.VisionThread;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class VisionThreadSensor {
    private VisionThread visionThread;
    private boolean hasTarget = false;
    private double distance = 0.0;
    private double angle = 0.0;
    public VisionThreadSensor(VideoSource videoSource, frc.vision.ContourDetection videopipeline) {
        visionThread = new VisionThread(videoSource, videopipeline, pipeline -> {    
        if (!pipeline.filterContoursOutput().isEmpty()) {
            Point point = getBestTargetPoint(pipeline.filterContoursOutput());
            double centeredReading = point.x - 320;
            angle = centeredReading;
            hasTarget = true;
        }
        else {
            hasTarget = false;
            angle = 0;
        }
        SmartDashboard.putBoolean(videoSource.getName() + " has Ring Target", hasTarget);
        if (hasTarget) {
            //System.out.println(videoSource.getName() + " has Ring Target: " + hasTarget);
        }
        SmartDashboard.putNumber(videoSource.getName() + " angle to ring", getAngle());
        });
        visionThread.start();
    }

    public Point getBestTargetPoint(ArrayList<MatOfPoint> ring) {
        // need a better algorithm.  Maybe find the biggest blob?
        return ring.get(0).toArray()[0];
    }

    public boolean hasTarget() {
        return hasTarget;
    }

    public double getDistance() {
        return distance;
    }

    public double getAngle() {
        return angle;
    }
}

package frc.robot.subsystems;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.Topic;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class LocationService extends SubsystemBase {
    private Field2d field = new Field2d();
        // get the default instance of NetworkTables
    private NetworkTableInstance inst = NetworkTableInstance.getDefault(); 

    public LocationService() {
        SmartDashboard.putData("Vision Location",field);
        System.out.println(inst.getHandle());
        
        System.out.println("Starting LocationService " + SmartDashboard.containsKey("AprilTag 0"));
        for (Topic topic : inst.getTopics()) {
            System.out.println("Starting LocationService NT " + topic.getName());
        }
    }

    /**
     * Tracks the location of the robot based on the cameras
     * 
     * @return Robot pose determined by the cameras.
     */
    public Pose2d getVisionLocation() {
        return field.getRobotPose();
    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run
        for(String key : SmartDashboard.getKeys()) {
        }
        for (int i=0; i<4; i++) {
            try {
                String keyName = "AprilTag "+i+" Pose";
                if(SmartDashboard.containsKey(keyName)){
                    double[] nums = {0,0,0,0,0,0};
                    nums = SmartDashboard.getNumberArray(keyName,nums);
                    Pose3d robotPose3d = readPose(nums);
                    Pose2d pose = robotPose3d.toPose2d();
                    // TODO: create an algorithm to pick the best camera location 
                    if(pose != null) {
                        this.field.setRobotPose(pose);
                    }
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
        
    }

    private Pose3d readPose(double[] nums) {
        if (nums.length==6) {
            return new Pose3d(nums[0], nums[1], nums[2], new Rotation3d(nums[3], nums[4], nums[5]));
        }
        return new Pose3d();
    }

}
